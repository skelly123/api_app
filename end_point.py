from flask import Flask, jsonify
import requests
import pandas as pd
from collections import OrderedDict

'''
This programme loads in data from a csv file, and an amazon page. Puts it into
a pandas dataframe, and then allows the data to be queried by an API with get
requests
'''

app = Flask(__name__)

# read in csv file
df_1 = pd.read_csv('products.csv')

# web request for other json data
r = requests.get('https://s3-eu-west-1.amazonaws.com/pricesearcher-code-tests/python-software-developer/products.json')
df_2 = pd.DataFrame(r.json())


# match csv file column names to amazon json
df_1.rename(columns={'Id': 'id', ' Name': 'name', ' Brand': 'brand', ' Retailer': 'retailer',
                     ' Price': 'price', ' InStock': 'in_stock'}, inplace=True)

# contcatenate dataframes
main_df = pd.concat([df_1, df_2])


# clean into id, name, brand, retailer, price, in_stock
def clean_response(item):
    # check for empty fields and replace extra quotes
    for key, value in item.items():
        if is_empty(value):
            item[key] = "null"
        if type(value) == str:
            item[key] = value.replace(r'\\', '').replace('\"', '').replace(' ', '')

    # check in_stock and turn into boolean
    if item['in_stock'].lower().replace(' ', '') in ['y', 'yes']:
        item['in_stock'] = True
    elif item['in_stock'].lower().replace(' ', '') in ['n', 'no']:
        item['in_stock'] = False
    else:
        item['in_stock'] = None

    # turn prices into floats
    try:
        item['price'] = float(item['price'])
    except Exception as e:
        print('cant do price')

    # turn ids into strings
    try:
        item['id'] = str(item['id'])
    except Exception as e:
        print('cant do ids')
    return item


# check if field is empty
def is_empty(item):
    if type(item) == str:
        if item.replace(' ', '') == '':
            return True
        else:
            return False
    return False


# turns output into correct order
def reorder(item):
    item = OrderedDict(item)
    for key in ['id', 'name', 'brand', 'retailer', 'price', 'in_stock']:
        item[key] = item.pop(key)
    return item


# pandas includes index, this is to remove it
def turn_into_normal_dict(item):
    new_item = {}
    for key, value in item.items():
        new_item[key] = list(value.values())[0]
    return new_item


# find in dataframe
def get_item(id):
    try:
        item = main_df.loc[main_df['id'] == int(id)].to_dict()
    except Exception as e:
        print('item not found csv input')
        try:
            item = main_df.loc[main_df['id'] == id].to_dict()
        except Exception as e:
            print('item not found amazon list')

    # return if item not found
    if all(value == {} for value in item.values()):
        return 'Item not found'

    # clean item
    try:
        item = turn_into_normal_dict(item)
        item = clean_response(item)
        item = reorder(item)
    except Exception as e:
        print('Cleaning problem')
    return item


@app.route('/<string:id>', methods=['GET'])
def hello_world(id):
    # find item
    item = get_item(id)
    return jsonify(item)
