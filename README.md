This programme is an API which can recieve get requests to recieve data about items


To run the programme:
move into the folder
enter the virtual environment using ```source env/bin/activate```
then run the flask script using ```FLASK_APP=end_point.py flask run```

Once the programme is running go to the adress printed out on the terminal
probably ```http://127.0.0.1:5000/```

Then to check a value by its id simply type in the url bar of your browser ```http://127.0.0.1:5000/id```

It's late, and I am feeling hungry so my favourite gif at the moment has to be
https://giphy.com/gifs/hungry-CDpAmfo9dbOyA